package com.turkcell.communicationroadmap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.turkcell.communicationroadmap.model.entity.Timeline;

@Repository
public interface TimelineRepository extends JpaRepository<Timeline, Long> {


    @Query(value = "SELECT * FROM (SELECT * FROM MAREX.T_ROADMAP_TIMELINES ORDER BY CREATE_DATE DESC) WHERE ROWNUM = 1", nativeQuery = true)
    Timeline findLastOne();


    @Query(value = "SELECT * FROM MAREX.T_ROADMAP_TIMELINES WHERE EXTRACT(YEAR FROM CREATE_DATE) = EXTRACT(YEAR FROM SYSDATE)", nativeQuery = true)
    List<Timeline> findActiveList();


}

