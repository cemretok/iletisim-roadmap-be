package com.turkcell.communicationroadmap.repository;

import java.util.List;

import com.turkcell.communicationroadmap.model.entity.Communication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.turkcell.communicationroadmap.model.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query(value = "SELECT A.* FROM T_ROADMAP_PRODUCTS A WHERE EXTRACT(YEAR FROM A.START_DATE) = EXTRACT(YEAR FROM SYSDATE) AND :categoryIds LIKE CONCAT('%', CONCAT(TO_CHAR(CATEGORY_ID), ';%')) AND :typeIds LIKE CONCAT('%', CONCAT(TO_CHAR(TYPE_ID), ';%'))", nativeQuery = true)
    List<Product> findProductByCategoryAndType(@Param("categoryIds") String categoryIds, @Param("typeIds") String typeIds);

    List<Product> findByCategory(Communication category);

    List<Product> findByType(Communication type);

}
