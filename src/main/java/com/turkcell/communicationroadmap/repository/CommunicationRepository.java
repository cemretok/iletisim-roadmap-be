package com.turkcell.communicationroadmap.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.turkcell.communicationroadmap.model.entity.Communication;

@Repository
public interface CommunicationRepository extends JpaRepository<Communication, Long> {
	 List<Communication> findByType(String type);
}

