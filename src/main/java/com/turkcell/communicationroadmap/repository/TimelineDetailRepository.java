package com.turkcell.communicationroadmap.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.turkcell.communicationroadmap.model.entity.TimelineDetail;

@Repository
public interface TimelineDetailRepository extends JpaRepository<TimelineDetail, Long> {
	
}

