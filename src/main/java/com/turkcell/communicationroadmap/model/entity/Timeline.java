package com.turkcell.communicationroadmap.model.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.turkcell.communicationroadmap.util.AppConstants;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "MAREX.T_ROADMAP_TIMELINES")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Timeline {

    @Id
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CREATE_DATE")
    @JsonFormat(pattern = AppConstants.DATE_FORMAT_FULL, timezone = AppConstants.TIMEZONE)
    private Date createDate;

    @JsonManagedReference
    @OneToMany(mappedBy = "timeline")
    private Set<TimelineDetail> timelineDetails;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Set<TimelineDetail> getTimelineDetails() {
        return timelineDetails;
    }

    public void setTimelineDetails(Set<TimelineDetail> timelineDetails) {
        this.timelineDetails = timelineDetails;
    }

    @Override
    public String toString() {
        return "[" + id + "," + name + "," + createDate + "," + timelineDetails + "]";
    }
}
