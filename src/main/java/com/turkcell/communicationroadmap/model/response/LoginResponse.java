package com.turkcell.communicationroadmap.model.response;

import java.io.Serializable;
import java.util.Objects;

public class LoginResponse implements Serializable {
    private static final long serialVersionUID = 443846971044647757L;

    private String userType;

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public LoginResponse userType(String userType){
        this.userType = userType;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoginResponse response = (LoginResponse) o;
        return Objects.equals(userType, response.userType);
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "userType='" + userType + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(userType);
    }
}
