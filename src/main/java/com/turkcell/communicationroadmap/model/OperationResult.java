package com.turkcell.communicationroadmap.model;

public class OperationResult {

	private boolean success;
	private String returnCode;
	private String returnMsg;
	private Object data;

	public OperationResult() {
		this.success = false;
	}

	public OperationResult(boolean success, String returnCode, String returnMsg, Object data) {
		super();
		this.success = success;
		this.returnCode = returnCode;
		this.returnMsg = returnMsg;
		this.data = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMsg() {
		return returnMsg;
	}

	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
