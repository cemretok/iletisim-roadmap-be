package com.turkcell.communicationroadmap.util;

public class AppConstants {

	public static final String COMM_TYPE_CATEGORY = "CATEGORY";
	public static final String COMM_TYPE_TYPE 	= "TYPE";
	public static final String DATE_FORMAT = "dd-MM-yyyy";
	public static final String DATE_FORMAT_FULL = "dd-MM-yyyy HH:mm:ss";
	public static final String RETURN_CODE_SUCCESS 	= "0";
	public static final String RETURN_CODE_ERROR 	= "1";
	public static final String RETURN_MSG_ERROR_DEFAULT 			= "An error occured...";
	public static final String RETURN_MSG_USER_NOT_FOUND = "User not found.";
	public static final String RETURN_MSG_INVALID_PASSWORD = "Invalid password.";
	public static final String TIMEZONE = "Europe/Istanbul";
	public static final String RETURN_MSG_PRODUCT_FOUND  = "Assigned product(s) found for given category or type.";
	private AppConstants() {
	}
}
