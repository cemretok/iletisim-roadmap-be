package com.turkcell.communicationroadmap.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class GlobalUtil {

	
	
	public static String exceptionToString(Exception e) {
		StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        return sw.toString();
	}
	
}
