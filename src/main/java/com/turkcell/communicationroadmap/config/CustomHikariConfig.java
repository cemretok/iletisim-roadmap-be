package com.turkcell.communicationroadmap.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@ConfigurationProperties(prefix = "spring.datasource.hikari")
public class CustomHikariConfig extends HikariConfig {

    @Bean
    public DataSource dataSource() {
        this.setJdbcUrl("jdbc:oracle:thin:@rcdbdev.tm.turkcell.tgc:1521:RCDBDEV");
        this.setUsername("marex");
        this.setPassword("d_marex");
        this.setConnectionTimeout(60000);
        return new HikariDataSource(this);
    }


}
