package com.turkcell.communicationroadmap.service;

import java.util.List;

import com.turkcell.communicationroadmap.model.entity.Communication;
import com.turkcell.communicationroadmap.model.OperationResult;

public interface CommunicationService {

    OperationResult add(Communication communication);

    OperationResult update(Communication communication);

    OperationResult delete(Long id);

    List<Communication> findByType(String type);

    Communication findById(Long id);
}
