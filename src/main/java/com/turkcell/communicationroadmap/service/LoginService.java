package com.turkcell.communicationroadmap.service;

import com.turkcell.communicationroadmap.model.response.ServiceResponse;
import com.turkcell.communicationroadmap.model.request.LoginRequest;

public interface LoginService {
    ServiceResponse loginWithBasicAuth(LoginRequest request);
}
