package com.turkcell.communicationroadmap.service.delegate.impl;

import com.turkcell.communicationroadmap.model.entity.Communication;
import com.turkcell.communicationroadmap.model.entity.Product;
import com.turkcell.communicationroadmap.model.entity.Timeline;
import com.turkcell.communicationroadmap.model.entity.TimelineDetail;
import com.turkcell.communicationroadmap.service.delegate.ExcelDelegateService;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;


@Service
public class ExcelDelegateServiceImpl implements ExcelDelegateService {

    private List<String> monthList;
    private List<String> weekList;

    public ExcelDelegateServiceImpl() {
        this.monthList = generateMonthList();
        this.weekList = generateWeekList();
    }

    @Override
    public ByteArrayInputStream generateTimelineExcel(Timeline timeline) {

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (Workbook workbook = new XSSFWorkbook()) {
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.WHITE.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            headerCellStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
            headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            headerCellStyle.setBorderTop(BorderStyle.MEDIUM);
            headerCellStyle.setTopBorderColor(IndexedColors.WHITE.getIndex());
            headerCellStyle.setBorderBottom(BorderStyle.MEDIUM);
            headerCellStyle.setBottomBorderColor(IndexedColors.WHITE.getIndex());
            headerCellStyle.setBorderLeft(BorderStyle.MEDIUM);
            headerCellStyle.setLeftBorderColor(IndexedColors.WHITE.getIndex());
            headerCellStyle.setBorderRight(BorderStyle.MEDIUM);
            headerCellStyle.setRightBorderColor(IndexedColors.WHITE.getIndex());

            Map<Communication, List<Product>> typeProductListMap = new HashMap<>();
            for (TimelineDetail timelineDetail : timeline.getTimelineDetails()) {
                Product product = timelineDetail.getProduct();
                if (typeProductListMap.get(product.getType()) == null) {
                    List<Product> productList = new ArrayList<>();
                    productList.add(product);
                    typeProductListMap.put(product.getType(), productList);
                } else {
                    typeProductListMap.get(product.getType()).add(product);
                }
            }
            for (Map.Entry<Communication, List<Product>> entry : typeProductListMap.entrySet()) {
                prepareSheetForCommunicationType(workbook, entry.getKey(), entry.getValue(), monthList, weekList, headerCellStyle);
            }

            workbook.write(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ByteArrayInputStream(out.toByteArray());
    }

    private void prepareSheetForCommunicationType(Workbook workbook, Communication type, List<Product> productList, List<String> monthList, List<String> weekList, CellStyle headerCellStyle) {
        Sheet sheet = workbook.createSheet(type.getName());
        int rowIndex = 0;

        //Month Row
        Row monthRow = sheet.createRow(rowIndex);
        monthRow.createCell(0).setCellValue(type.getName() + " ROADMAP");
        sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 0));
        CellUtil.setVerticalAlignment(CellUtil.getCell(monthRow, 0), VerticalAlignment.CENTER);
        for (int i = 0; i < monthList.size(); i++) {
            int columnIndex = i * 4;
            Cell cell = monthRow.createCell(columnIndex + 1);
            cell.setCellValue(monthList.get(i));
            cell.setCellStyle(headerCellStyle);
            sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, columnIndex + 1, columnIndex + 4));
            CellUtil.setAlignment(cell, HorizontalAlignment.CENTER);

        }
        rowIndex++;

        //Week Row
        Row weekRow = sheet.createRow(rowIndex);
        for (int i = 0; i < weekList.size(); i++) {
            Cell cell = weekRow.createCell(i + 1);
            cell.setCellValue(weekList.get(i));
            cell.setCellStyle(headerCellStyle);
        }
        rowIndex++;

        Map<Communication, List<Product>> categoryProductListMap = new HashMap<>();
        for (Product product : productList) {
            if (categoryProductListMap.get(product.getCategory()) == null) {
                List<Product> productSubList = new ArrayList<>();
                productSubList.add(product);
                categoryProductListMap.put(product.getCategory(), productSubList);
            } else {
                categoryProductListMap.get(product.getCategory()).add(product);
            }
        }

        for (Map.Entry<Communication, List<Product>> entry : categoryProductListMap.entrySet()) {
            rowIndex = prepareRowForCommunicationCategory(workbook, sheet, rowIndex, entry.getKey(), entry.getValue());

        }

        for (int i = 1; i < 48; i++) {
            sheet.autoSizeColumn(i);
        }

    }

    private int prepareRowForCommunicationCategory(Workbook workbook, Sheet sheet, int startRowIndex, Communication category, List<Product> productList) {

        int rowIndex = startRowIndex;
        Row categoryNameRow = sheet.createRow(rowIndex);

        Cell categoryNameCell = categoryNameRow.createCell(0);
        categoryNameCell.setCellValue(category.getName());
        Font categoryNameFont = workbook.createFont();
        categoryNameFont.setBold(true);
        categoryNameFont.setColor(IndexedColors.WHITE.getIndex());

        CellStyle categoryNameCellStyle = workbook.createCellStyle();
        categoryNameCellStyle.setFont(categoryNameFont);
        categoryNameCellStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
        categoryNameCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        categoryNameCell.setCellStyle(categoryNameCellStyle);
        rowIndex++;

        for (Product product : productList) {
            Row productRow = sheet.createRow(rowIndex);
            Calendar cal = Calendar.getInstance();
            cal.setTime(product.getStartDate());
            int startCellIndex = (cal.get(Calendar.MONTH) * 4) + (cal.get(Calendar.DAY_OF_MONTH) / 7) + 1;
            cal.setTime(product.getEndDate());
            int endCellIndex = (cal.get(Calendar.MONTH) * 4) + (cal.get(Calendar.DAY_OF_MONTH) / 7) + 1;

            CellStyle productCellStyle = generateProductCellStyle(workbook, product.getColor());
            if ("gray".equals(product.getColor())) {
                productCellStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
            } else {
                productCellStyle.setFillForegroundColor(IndexedColors.valueOf(product.getColor().toUpperCase()).getIndex());
            }
            productCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            Cell productCell = productRow.createCell(startCellIndex);
            productCell.setCellValue(product.getName());
            productCell.setCellStyle(productCellStyle);
            if (startCellIndex != endCellIndex) {
                sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, startCellIndex, endCellIndex));
            }
            rowIndex++;
        }

        sheet.addMergedRegion(new CellRangeAddress(startRowIndex, rowIndex - 1, 0, 0));
        CellUtil.setVerticalAlignment(CellUtil.getCell(categoryNameRow, 0), VerticalAlignment.CENTER);
        return rowIndex;


    }

    private CellStyle generateProductCellStyle(Workbook workbook, String color) {
        CellStyle cellStyle = workbook.createCellStyle();
        Font productFont = workbook.createFont();
        switch (color) {
            case "black":
                productFont.setColor(IndexedColors.WHITE.getIndex());
                cellStyle.setFont(productFont);
                cellStyle.setFillForegroundColor(IndexedColors.BLACK.getIndex());
                break;
            case "white":
                cellStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
                break;
            case "red":
                cellStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
                break;
            case "yellow":
                cellStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
                break;
            case "green":
                cellStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
                break;
            case "orange":
                cellStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
                break;
            case "pink":
                cellStyle.setFillForegroundColor(IndexedColors.PINK.getIndex());
                break;
            case "blue":
                cellStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
                break;
            case "purple":
                cellStyle.setFillForegroundColor(IndexedColors.VIOLET.getIndex());
                break;
            case "gray":
                cellStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
                break;
            case "brown":
                cellStyle.setFillForegroundColor(IndexedColors.BROWN.getIndex());
                break;
            default:
                cellStyle.setFillForegroundColor(IndexedColors.SEA_GREEN.getIndex());
                break;
        }
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        return cellStyle;
    }


    private List<String> generateMonthList() {
        List<String> months = new ArrayList<>();
        months.add("OCAK");
        months.add("ŞUBAT");
        months.add("MART");
        months.add("NİSAN");
        months.add("MAYIS");
        months.add("HAZİRAN");
        months.add("TEMMUZ");
        months.add("AĞUSTOS");
        months.add("EYLÜL");
        months.add("EKİM");
        months.add("KASIM");
        months.add("ARALIK");
        return months;
    }

    private List<String> generateWeekList() {
        List<String> weeks = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            weeks.add("W1");
            weeks.add("W2");
            weeks.add("W3");
            weeks.add("W4");
        }
        return weeks;
    }
}
