package com.turkcell.communicationroadmap.service.delegate;

import com.turkcell.communicationroadmap.model.entity.Timeline;

import java.io.ByteArrayInputStream;

public interface ExcelDelegateService {
    ByteArrayInputStream generateTimelineExcel(Timeline timeline);
}
