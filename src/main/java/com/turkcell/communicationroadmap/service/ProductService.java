package com.turkcell.communicationroadmap.service;

import java.util.List;

import com.turkcell.communicationroadmap.model.OperationResult;
import com.turkcell.communicationroadmap.model.entity.Communication;
import com.turkcell.communicationroadmap.model.entity.Product;

public interface ProductService {

    List<Product> inquiryByCommunications(List<Communication> commList);

    List<Product> findAll();

    Product findById(Long id);

    OperationResult add(Product product);

    OperationResult update(Product product);

    OperationResult delete(Long id);

}
