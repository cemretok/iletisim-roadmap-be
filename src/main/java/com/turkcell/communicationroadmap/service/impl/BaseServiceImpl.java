package com.turkcell.communicationroadmap.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.turkcell.communicationroadmap.model.OperationResult;
import com.turkcell.communicationroadmap.util.AppConstants;
import com.turkcell.communicationroadmap.util.GlobalUtil;

public class BaseServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(BaseServiceImpl.class);
	
	public void updateResult(Exception e, OperationResult result) {
		String errorMsg = GlobalUtil.exceptionToString(e);
		result.setReturnCode(AppConstants.RETURN_CODE_ERROR);
		result.setReturnMsg(e.toString());
		
		logger.error(errorMsg);
	}
}
