package com.turkcell.communicationroadmap.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.turkcell.communicationroadmap.model.entity.Communication;
import com.turkcell.communicationroadmap.util.AppConstants;
import org.springframework.stereotype.Service;

import com.turkcell.communicationroadmap.model.OperationResult;
import com.turkcell.communicationroadmap.model.entity.Product;
import com.turkcell.communicationroadmap.repository.ProductRepository;
import com.turkcell.communicationroadmap.service.ProductService;

@Service
public class ProductServiceImpl extends BaseServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> inquiryByCommunications(List<Communication> commList) {
        String categoryIds = "";
        String typeIds = "";

        for (Communication communication : commList) {
            if (communication.getType().equals(AppConstants.COMM_TYPE_CATEGORY)) {
                categoryIds += communication.getId() + ";";
            } else if (communication.getType().equals(AppConstants.COMM_TYPE_TYPE)) {
                typeIds += communication.getId() + ";";
            }
        }

        System.out.println("Sayi: " + categoryIds + " " + typeIds);
        List<Product> productByCategoryAndType = productRepository.findProductByCategoryAndType(categoryIds, typeIds);
        List<Product> lastOnes = new ArrayList<>();
        Set<String> productNames = new HashSet<>();
        for (Product product : productByCategoryAndType) {
            productNames.add(product.getName());
        }

        for (String productName : productNames) {
            lastOnes.add(getLastOneForProductName(productByCategoryAndType, productName));
        }
        return lastOnes;
    }

    private Product getLastOneForProductName(List<Product> productByCategoryAndType, String productName) {
        Product found = new Product();
        found.setId(0L);
        for (Product product : productByCategoryAndType) {
            if(productName.equals(product.getName()) && (found.getId() < product.getId())){
                found = product;
            }
        }

        return found;
    }

    @Override
    public List<Product> findAll() {
        return (List<Product>) productRepository.findAll();
    }

    @Override
    public Product findById(Long id) {
        return productRepository.getOne(id);
    }

    @Override
    public OperationResult add(Product product) {
        return save(product);
    }

    @Override
    public OperationResult update(Product productNew) {

        Product product = productRepository.getOne(productNew.getId());
        product.setBudget(productNew.getBudget());
        product.setCategory(productNew.getCategory());
        product.setColor(productNew.getColor());
        product.setDescription(productNew.getDescription());
        product.setEndDate(productNew.getEndDate());
        product.setName(productNew.getName());
        product.setStartDate(productNew.getStartDate());
        product.setType(productNew.getType());
        product.setFileName(productNew.getFileName());
        product.setFile(productNew.getFile());
        product.setRequestOwner(productNew.getRequestOwner());

        return save(product);
    }

    @Override
    public OperationResult delete(Long id) {
        OperationResult result = new OperationResult();
        try {
            productRepository.delete(id);
            result.setSuccess(true);
        } catch (Exception e) {
            updateResult(e, result);
        }
        return result;
    }

    private OperationResult save(Product product) {
        OperationResult result = new OperationResult();
        try {
            Product data = productRepository.save(product);
            result.setSuccess(true);
            result.setData(data);
        } catch (Exception e) {
            updateResult(e, result);
        }
        return result;
    }

}
