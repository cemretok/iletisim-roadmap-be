package com.turkcell.communicationroadmap.service.impl;

import java.io.ByteArrayInputStream;

import java.util.Date;
import java.util.List;

import com.turkcell.communicationroadmap.model.entity.Product;
import com.turkcell.communicationroadmap.model.entity.TimelineDetail;
import com.turkcell.communicationroadmap.service.ProductService;
import com.turkcell.communicationroadmap.service.TimelineDetailService;
import com.turkcell.communicationroadmap.service.delegate.ExcelDelegateService;
import org.springframework.stereotype.Service;

import com.turkcell.communicationroadmap.model.OperationResult;
import com.turkcell.communicationroadmap.model.entity.Timeline;
import com.turkcell.communicationroadmap.repository.TimelineRepository;
import com.turkcell.communicationroadmap.service.TimelineService;

@Service
public class TimelineServiceImpl extends BaseServiceImpl implements TimelineService {

    private final ProductService productService;
    private final TimelineDetailService timelineDetailService;
    private final ExcelDelegateService excelDelegateService;
    private final TimelineRepository timelineRepository;

    public TimelineServiceImpl(ProductService productService, TimelineDetailService timelineDetailService, ExcelDelegateService excelDelegateService, TimelineRepository timelineRepository) {
        this.productService = productService;
        this.timelineDetailService = timelineDetailService;
        this.excelDelegateService = excelDelegateService;
        this.timelineRepository = timelineRepository;
    }

    @Override
    public List<Timeline> findAll() {
        return timelineRepository.findAll();
    }

    @Override
    public Timeline findById(Long id) {
        return timelineRepository.getOne(id);
    }

    @Override
    public Timeline findLastOne() {
        return timelineRepository.findLastOne();
    }

    @Override
    public List<Timeline> findActiveList() {
        return  timelineRepository.findActiveList();
    }

    @Override
    public OperationResult add(Timeline timeline) {
        OperationResult timelineOpResult = save(timeline);

        if (timelineOpResult.isSuccess()) {

            for (TimelineDetail detail : timeline.getTimelineDetails()) {
                Product productOld = productService.findById(detail.getProduct().getId());
                Product product = null;
                try {
                    product = (Product) productOld.clone();
                } catch (CloneNotSupportedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                product.setId(null);
                product.setStartDate(detail.getProduct().getStartDate());
                product.setEndDate(detail.getProduct().getEndDate());


                OperationResult productOpResult = productService.add(product);

                if (productOpResult.isSuccess()) {

                    TimelineDetail timelineDetail = new TimelineDetail();
                    timelineDetail.setProduct((Product) productOpResult.getData());
                    timelineDetail.setTimeline((Timeline) timelineOpResult.getData());

                    timelineDetailService.add(timelineDetail);
                }
            }

            Timeline data = findById(((Timeline) timelineOpResult.getData()).getId());
            timelineOpResult.setData(data);
        }
        return timelineOpResult;
    }

    @Override
    public OperationResult update(Timeline timelineNew) {
        Timeline timeline = timelineRepository.getOne(timelineNew.getId());
        timeline.setCreateDate(new Date());
        timeline.setName(timelineNew.getName());
        timeline.setTimelineDetails(timelineNew.getTimelineDetails());
        return save(timeline);
    }

    @Override
    public OperationResult delete(Long id) {
        OperationResult result = new OperationResult();
        try {
            timelineRepository.delete(id);
            result.setSuccess(true);
        } catch (Exception e) {
            updateResult(e, result);
        }
        return result;
    }

    @Override
    public ByteArrayInputStream generateExcel(Long timelineId) {
        Timeline timeline = findById(timelineId);
        return excelDelegateService.generateTimelineExcel(timeline);
    }

    private OperationResult save(Timeline timeline) {
        OperationResult result = new OperationResult();
        try {
            Timeline data = timelineRepository.save(timeline);
            result.setSuccess(true);
            result.setData(data);
        } catch (Exception e) {
            updateResult(e, result);
        }
        return result;
    }

}
