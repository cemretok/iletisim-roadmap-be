package com.turkcell.communicationroadmap.service.impl;

import com.turkcell.communicationroadmap.model.response.ServiceResponse;
import com.turkcell.communicationroadmap.model.entity.User;
import com.turkcell.communicationroadmap.model.request.LoginRequest;
import com.turkcell.communicationroadmap.model.response.LoginResponse;
import com.turkcell.communicationroadmap.repository.UserRepository;
import com.turkcell.communicationroadmap.service.LoginService;
import com.turkcell.communicationroadmap.util.AppConstants;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {
    private final UserRepository userRepository;

    public LoginServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public ServiceResponse loginWithBasicAuth(LoginRequest request) {
        ServiceResponse serviceResponse = new ServiceResponse();
        User found = userRepository.findByName(request.getUserName());

        if (null == found) {
            serviceResponse.setReturnMsg(AppConstants.RETURN_MSG_USER_NOT_FOUND);
            return serviceResponse;
        }

        if (!DigestUtils.sha256Hex(request.getPass()).equals(found.getHash())) {
            serviceResponse.setReturnMsg(AppConstants.RETURN_MSG_INVALID_PASSWORD);
            return serviceResponse;
        }

        serviceResponse.setData(new LoginResponse().userType(found.getType()));
        return serviceResponse;
    }
}
