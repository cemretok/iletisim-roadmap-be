package com.turkcell.communicationroadmap.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.turkcell.communicationroadmap.model.entity.Product;
import com.turkcell.communicationroadmap.repository.ProductRepository;
import com.turkcell.communicationroadmap.util.AppConstants;
import org.springframework.stereotype.Service;

import com.turkcell.communicationroadmap.model.entity.Communication;
import com.turkcell.communicationroadmap.model.OperationResult;
import com.turkcell.communicationroadmap.repository.CommunicationRepository;
import com.turkcell.communicationroadmap.service.CommunicationService;
import org.springframework.util.CollectionUtils;

@Service
public class CommunicationServiceImpl extends BaseServiceImpl implements CommunicationService {

	private final CommunicationRepository communicationRepository;
	private final ProductRepository productRepository;

	public CommunicationServiceImpl(CommunicationRepository communicationRepository, ProductRepository productRepository) {
		this.communicationRepository = communicationRepository;
		this.productRepository = productRepository;
	}

	@Override
	public OperationResult add(Communication communication) {
		return addUpdate(communication);
	}

	@Override
	public OperationResult update(Communication communicationNew) {

		Communication communication = communicationRepository.getOne(communicationNew.getId());
		communication.setName(communicationNew.getName());

		return addUpdate(communication);
	}

	@Override
	public OperationResult delete(Long id) {
		OperationResult result = new OperationResult();
		Communication toBeDeleted = communicationRepository.getOne(id);
		List<Product> assignedProducts = new ArrayList<>();
		if (toBeDeleted.getType().equals(AppConstants.COMM_TYPE_CATEGORY)){
			assignedProducts = productRepository.findByCategory(toBeDeleted);
		} else if (toBeDeleted.getType().equals(AppConstants.COMM_TYPE_TYPE)){
			assignedProducts = productRepository.findByType(toBeDeleted);
		}
		if(!CollectionUtils.isEmpty(assignedProducts)){
			result.setReturnCode(AppConstants.RETURN_CODE_ERROR);
			result.setReturnMsg(AppConstants.RETURN_MSG_PRODUCT_FOUND);
			return result;
		}
		try {
			communicationRepository.delete(id);
			result.setSuccess(true);
		} catch (Exception e) {
			updateResult(e, result);
		}
		return result;	
	}

	private OperationResult addUpdate(Communication communication) {
		OperationResult result = new OperationResult();
		try {
			Communication data = communicationRepository.save(communication);
			result.setSuccess(true);
			result.setData(data);
		} catch (Exception e) {
			updateResult(e, result);			
		}
		return result;
	}

	@Override
	public List<Communication> findByType(String type) {
		return communicationRepository.findByType(type);
	}

	@Override
	public Communication findById(Long id) {
		return communicationRepository.getOne(id);
	}
}
