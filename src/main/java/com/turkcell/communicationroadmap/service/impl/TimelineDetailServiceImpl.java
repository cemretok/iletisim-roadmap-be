package com.turkcell.communicationroadmap.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.turkcell.communicationroadmap.model.OperationResult;
import com.turkcell.communicationroadmap.model.entity.TimelineDetail;
import com.turkcell.communicationroadmap.repository.TimelineDetailRepository;
import com.turkcell.communicationroadmap.service.TimelineDetailService;

@Service
public class TimelineDetailServiceImpl extends BaseServiceImpl implements TimelineDetailService {


	private final TimelineDetailRepository timelineDetailRepository;

	public TimelineDetailServiceImpl(TimelineDetailRepository timelineDetailRepository) {
		this.timelineDetailRepository = timelineDetailRepository;
	}

	@Override
	public OperationResult add(TimelineDetail timelineDetail) {
		return addUpdate(timelineDetail);
	}

	@Override
	public OperationResult update(TimelineDetail timelineDetailNew) {
		return null;
	}

	@Override
	public OperationResult delete(Long id) {
		OperationResult result = new OperationResult();
		try {
			timelineDetailRepository.delete(id);
			result.setSuccess(true);
		} catch (Exception e) {
			updateResult(e, result);
		}
		return result;	
	}

	private OperationResult addUpdate(TimelineDetail timelineDetail) {
		OperationResult result = new OperationResult();
		try {
			TimelineDetail data = timelineDetailRepository.save(timelineDetail);
			result.setSuccess(true);
			result.setData(data);
		} catch (Exception e) {
			updateResult(e, result);						
		}
		return result;
	}


	@Override
	public TimelineDetail findById(Long id) {
		return timelineDetailRepository.getOne(id);
	}

	@Override
	public List<TimelineDetail> findAll() {
		return  timelineDetailRepository.findAll();
	}
}
