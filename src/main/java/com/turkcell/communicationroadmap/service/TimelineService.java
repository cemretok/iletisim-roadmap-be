package com.turkcell.communicationroadmap.service;

import java.io.ByteArrayInputStream;
import java.util.List;

import com.turkcell.communicationroadmap.model.OperationResult;
import com.turkcell.communicationroadmap.model.entity.Timeline;

public interface TimelineService {

    List<Timeline> findAll();

    Timeline findById(Long id);

    Timeline findLastOne();

    List<Timeline> findActiveList();

    OperationResult add(Timeline timeline);

    OperationResult update(Timeline timeline);

    OperationResult delete(Long id);

    ByteArrayInputStream generateExcel(Long timelineId);

}
