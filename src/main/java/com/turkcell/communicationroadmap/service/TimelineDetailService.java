package com.turkcell.communicationroadmap.service;

import java.util.List;

import com.turkcell.communicationroadmap.model.OperationResult;
import com.turkcell.communicationroadmap.model.entity.TimelineDetail;

public interface TimelineDetailService {

    OperationResult add(TimelineDetail timelineDetail);

    OperationResult update(TimelineDetail timelineDetail);

    OperationResult delete(Long id);

    TimelineDetail findById(Long id);

    List<TimelineDetail> findAll();
}
