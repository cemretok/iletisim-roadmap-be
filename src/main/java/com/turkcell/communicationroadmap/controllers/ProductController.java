package com.turkcell.communicationroadmap.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import com.turkcell.communicationroadmap.model.entity.Communication;
import com.turkcell.communicationroadmap.model.OperationResult;
import com.turkcell.communicationroadmap.model.entity.Product;
import com.turkcell.communicationroadmap.model.response.ServiceResponse;
import com.turkcell.communicationroadmap.service.ProductService;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/inquiryByCategoryAndType")
    public ServiceResponse inquiryProductsByCommunications(@RequestBody List<Communication> commList) {
        logger.info("new call to /api/product/inquiryByCategoryAndType endpoint");
        ServiceResponse response = new ServiceResponse();
        List<Product> products = productService.inquiryByCommunications(commList);
        response.setData(products);
        return response;
    }

    @GetMapping("/inquiry")
    public ServiceResponse inquiryAllProducts() {
        logger.info("new call to /api/product/inquiry endpoint");
        ServiceResponse response = new ServiceResponse();
        List<Product> list = productService.findAll();
        response.setData(list);
        return response;
    }

    @GetMapping("/inquiryById/{id}")
    public ServiceResponse findProductById(@PathVariable("id") Long id) {
        logger.info("new call to /api/product/inquiryById endpoint");
        ServiceResponse response = new ServiceResponse();
        Product product = productService.findById(id);
        response.setData(product);
        return response;
    }

    @PostMapping("/add")
    public ServiceResponse addProduct(@RequestBody Product product) {
        logger.info("new call to /api/product/add endpoint");
        OperationResult result = productService.add(product);
        return new ServiceResponse(result);
    }

    @PostMapping("/update")
    public ServiceResponse updateProduct(@RequestBody Product product) {
        logger.info("new call to /api/product/update endpoint");
        OperationResult result = productService.update(product);
        return new ServiceResponse(result);
    }

    @PostMapping("/delete")
    public ServiceResponse deleteProduct(@RequestBody Product product) {
        logger.info("new call to /api/product/delete endpoint");
        OperationResult result = productService.delete(product.getId());
        return new ServiceResponse(result);
    }

}
