package com.turkcell.communicationroadmap.controllers;

import java.io.ByteArrayInputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.turkcell.communicationroadmap.model.OperationResult;
import com.turkcell.communicationroadmap.model.response.ServiceResponse;
import com.turkcell.communicationroadmap.model.entity.Timeline;
import com.turkcell.communicationroadmap.service.TimelineService;

@RestController
@RequestMapping("/api/timeline")
public class TimelineController {

    private static final Logger logger = LoggerFactory.getLogger(TimelineController.class);
    private final TimelineService timelineService;

    public TimelineController(TimelineService timelineService) {
        this.timelineService = timelineService;
    }

    @GetMapping("/inquiry")
    public ServiceResponse inquiryTimeline() {
        logger.info("new call to /api/timeline/inquiry endpoint");
        ServiceResponse response = new ServiceResponse();
        List<Timeline> list = timelineService.findAll();
        response.setData(list);
        return response;
    }

    @GetMapping("/inquiryById")
    public ServiceResponse inquiryTimelineById(@RequestParam Long id) {
        logger.info("new call to /api/timeline/inquiryById endpoint");
        ServiceResponse response = new ServiceResponse();
        Timeline timeline = timelineService.findById(id);
        response.setData(timeline);
        return response;
    }

    @GetMapping("/inquiryLastOne")
    public ServiceResponse inquiryTimelineLastOne() {
        logger.info("new call to /api/timeline/inquiryLastOne endpoint");
        ServiceResponse response = new ServiceResponse();
        Timeline timeline = timelineService.findLastOne();
        response.setData(timeline);
        return response;
    }

    @GetMapping("/inquiryActiveList")
    public ServiceResponse inquiryTimelineCurrYear() {
        logger.info("new call to /api/timeline/inquiryActiveList endpoint");
        ServiceResponse response = new ServiceResponse();
        List<Timeline> list = timelineService.findActiveList();
        response.setData(list);
        return response;
    }

    @PostMapping("/add")
    public ServiceResponse add(@RequestBody Timeline timeline) {
        logger.info("new call to /api/timeline/add endpoint");
        OperationResult result = timelineService.add(timeline);
        return new ServiceResponse(result);
    }

    @PostMapping("/update")
    public ServiceResponse update(@RequestBody Timeline timeline) {
        logger.info("new call to /api/timeline/update endpoint");
        //TODO: implement or remove.
        return null;
    }

    @GetMapping(value = "/excel")
    public ResponseEntity<InputStreamResource> generateExcel(@RequestParam Long timelineId) {
        logger.info("new call to /api/timeline/excel endpoint");
        ByteArrayInputStream in = timelineService.generateExcel(timelineId);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=Timeline_" + timelineId + ".xlsx");
        return new ResponseEntity(new InputStreamResource(in), headers, HttpStatus.OK);
    }

}
