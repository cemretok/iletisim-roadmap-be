package com.turkcell.communicationroadmap.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.turkcell.communicationroadmap.model.entity.Communication;
import com.turkcell.communicationroadmap.model.OperationResult;
import com.turkcell.communicationroadmap.model.response.ServiceResponse;
import com.turkcell.communicationroadmap.service.CommunicationService;

@RestController
@RequestMapping("/api/communication")
public class CommunicationController {

    private static final Logger logger = LoggerFactory.getLogger(CommunicationController.class);
    private final CommunicationService communicationService;

    public CommunicationController(CommunicationService communicationService) {
        this.communicationService = communicationService;
    }

    @GetMapping("/inquiry")
    public ServiceResponse inquiryCommunication(@RequestParam("type") String type) {
        logger.info("new call to /api/communication/inquiry endpoint");
        ServiceResponse response = new ServiceResponse();
        List<Communication> list = communicationService.findByType(type);
        response.setData(list);
        return response;
    }

    @PostMapping("/add")
    public ServiceResponse add(@RequestBody Communication communication) {
        logger.info("new call to /api/communication/add endpoint");
        OperationResult result = communicationService.add(communication);
        return new ServiceResponse(result);
    }

    @PostMapping("/update")
    public ServiceResponse update(@RequestBody Communication communication) {
        logger.info("new call to /api/communication/update endpoint");
        OperationResult result = communicationService.update(communication);
        return new ServiceResponse(result);
    }

    @PostMapping("/delete")
    public ServiceResponse delete(@RequestBody Communication communication) {
        logger.info("new call to /api/communication/delete endpoint");
        OperationResult result = communicationService.delete(communication.getId());
        return new ServiceResponse(result);
    }

}
