package com.turkcell.communicationroadmap.controllers;

import com.turkcell.communicationroadmap.model.response.ServiceResponse;
import com.turkcell.communicationroadmap.model.request.LoginRequest;
import com.turkcell.communicationroadmap.service.LoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/login")
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
    private final LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/basicAuth")
    public ServiceResponse loginWithBasicAuth(@RequestBody LoginRequest request) {
        logger.info("new call to /api/login/basicAuth endpoint");
        return loginService.loginWithBasicAuth(request);
    }

}
