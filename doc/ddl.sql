--- MYSQL DIALECT ---
DROP TABLE IF EXISTS `t_roadmap_communications`;
DROP TABLE IF EXISTS `t_roadmap_products`;
DROP TABLE IF EXISTS `t_roadmap_timeline_details`;
DROP TABLE IF EXISTS `t_roadmap_timelines`;
DROP TABLE IF EXISTS `t_roadmap_users`;

CREATE TABLE `t_roadmap_communications` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(100) DEFAULT NULL,
    `type` varchar(10) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `unique_constraints-name_type` (`name`,`type`)
    ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

CREATE TABLE `t_roadmap_products` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(250) DEFAULT NULL,
    `start_date` date DEFAULT NULL,
    `end_date` date DEFAULT NULL,
    `budget` double DEFAULT NULL,
    `description` varchar(4000) DEFAULT NULL,
    `color` varchar(100) DEFAULT NULL,
    `category_id` int(11) DEFAULT NULL,
    `type_id` int(11) DEFAULT NULL,
    `file` mediumblob,
    `request_owner` varchar(100) DEFAULT NULL,
    `file_name` varchar(100) DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;

CREATE TABLE `t_roadmap_timeline_details` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `timeline_id` int(11) DEFAULT NULL,
    `product_id` int(11) DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=latin1;

CREATE TABLE `t_roadmap_timelines` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(250) DEFAULT NULL,
    `create_date` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

CREATE TABLE `t_roadmap_users` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL,
    `hash` varchar(64) DEFAULT NULL,
    `type` varchar(20) DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--- ORACLE DIALECT ---
DROP TABLE "MAREX"."T_ROADMAP_COMMUNICATIONS";
DROP TABLE "MAREX"."T_ROADMAP_PRODUCTS";
DROP TABLE "MAREX"."T_ROADMAP_TIMELINE_DETAILS";
DROP TABLE "MAREX"."T_ROADMAP_TIMELINES";
DROP TABLE "MAREX"."T_ROADMAP_USERS";

CREATE TABLE "MAREX"."T_ROADMAP_COMMUNICATIONS"
(
    "ID"   NUMBER,
    "NAME" VARCHAR2(100),
    "TYPE" VARCHAR2(10),
    CONSTRAINT "T_ROADMAP_COMMUNICATIONS_PK" PRIMARY KEY ("ID")
);

CREATE TABLE "MAREX"."T_ROADMAP_PRODUCTS"
(
    "ID"            NUMBER,
    "NAME"          VARCHAR2(250),
    "START_DATE"    DATE,
    "END_DATE"      DATE,
    "BUDGET"        FLOAT(126),
    "DESCRIPTION"   VARCHAR2(4000),
    "COLOR"         VARCHAR2(100),
    "CATEGORY_ID"   NUMBER,
    "TYPE_ID"       NUMBER,
    "FILE_NAME"     VARCHAR2(100),
    "FILE"          CLOB,
    "REQUEST_OWNER" VARCHAR2(100),
    CONSTRAINT "T_ROADMAP_PRODUCTS_PK" PRIMARY KEY ("ID")
);

CREATE TABLE "MAREX"."T_ROADMAP_TIMELINE_DETAILS"
(
    "ID"          NUMBER,
    "TIMELINE_ID" NUMBER,
    "PRODUCT_ID"  NUMBER,
    CONSTRAINT "T_ROADMAP_TIMELINE_DETAILS_PK" PRIMARY KEY ("ID")
);

CREATE TABLE "MAREX"."T_ROADMAP_TIMELINES"
(
    "ID"          NUMBER,
    "NAME"        VARCHAR2(250),
    "CREATE_DATE" DATE,
    CONSTRAINT "T_ROADMAP_TIMELINES_PK" PRIMARY KEY ("ID")
);

CREATE TABLE "MAREX"."T_ROADMAP_USERS"
(
    "ID"   NUMBER,
    "NAME" VARCHAR2(50),
    "HASH" VARCHAR2(64),
    "TYPE" VARCHAR2(20),
    CONSTRAINT "T_ROADMAP_USERS_PK" PRIMARY KEY ("ID")
);

